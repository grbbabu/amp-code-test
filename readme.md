# AMP code test

This test is designed to assess your skill level.

## The test should only take a maximum of 2 hours, don't spend any longer!

## We are looking at the following skills:

- Responsive design
- Sass / css
- Semantics
- Accessibility
- JavaScript
- AngularJS
- Automated testing of JavaScript
- Git
- Grunt or Gulp (we use gulp here)

Lastly, your ability to follow a brief or a spec.

## Stuff you can leave out

- minification & concatenation
- cross browser support
- end to end testing

## Functional requirements

1. We are wanting you to display a list of users with a filter. Preferably, the list will update whilst the user is typing or autocomplete if you will. 

We have supplied the JSON for you. It may or may not be correct. 

The page should be responsive. A creative has been supplied for the larger views, however, for the smaller views you'll need to use your judgement.

## javascript

If you need to install dependencies, servers or anything to run the project then please do so. Just bear in mind, that we are accessing your knowedge in this area. 

## Style

- DO NOT USE ANY OTHER LIBRARIES - use only what's included in the test, I was nice and included bootstrap for you.

## testing

- create a couple of tests, no more than that.

## Pre requisites

- node
- npm
- grunt

## Getting started

1. Fork this repository and create a branch named after yourself. ***Once your done you will need to create a pull request so we can view your changes.***

- Screen creative for desktop supplied under the creative folder.

## Commands

In order to run the project you'll need to use the following commands.

npm i

bower i

npm i karma-cli -g

## The end result should be:

Simple, testable & well formatted code & an application that can run.